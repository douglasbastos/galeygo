# -*- coding: utf-8 -*-
from django.contrib import admin

from galeygo_admin.admin.region import RegionAdmin
from galeygo_admin.models import Region

admin.site.register(Region, RegionAdmin)
